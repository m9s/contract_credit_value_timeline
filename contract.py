# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.pool import Pool


class Contract(ModelWorkflow, ModelSQL, ModelView):
    _name = 'contract.contract'

    def _create_invoice_line(self, line):
        account_obj = Pool().get('account.account')
        invoice_obj = Pool().get('account.invoice')

        invoice = invoice_obj.browse(line.get('invoice'))
        date = invoice.accounting_date
        line['account'] = account_obj.get_account_by_date(
            line.get('account'), date)

        return super(Contract, self)._create_invoice_line(line)

Contract()
