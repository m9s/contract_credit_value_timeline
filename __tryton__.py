# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Contract Credit Value Timeline',
    'name_de_DE': 'Vertragsverwaltung Guthabenverwaltung Gültigkeitsdauer',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides timeline features
    ''',
    'description_de_DE': '''
    - Stellt die Merkmale der Gültigkeitsdauermodule zur Verfügung
    ''',
    'depends': [
        'contract_automation_invoice_timeline',
        'contract_credit_value',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
